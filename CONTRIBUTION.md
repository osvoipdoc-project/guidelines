This file should serve as your template for contributing a new document to the repository. 

Every document must contain the following mandatory sections:

* Document Title
* General:Abstract
* General:Prerequisites
* General:Technical Data
* License

The license section should be recreated as stated below - unmodified.

---------------------------------------- CUT HERE ----------------------------------------

# Your document title here #

## General
### Abstract 
The abstract should contain 1 to 3 paragraphs, providing a general understanding of the document. The abstract should provide information about the rationale for the document and the basic problem it aims to solve.

### Prerequisites 
If you assume that the reader already knows something, you should detail these assumptions in this section. If you assume that the reader has no prior knowledge, simply state this section as “No prior prerequisites required”.

### Technical Data
If your document details various hardware/software related elements, these should be listed here. Namely, servers used, softwares installed, memory requirements, network requirements, etc. If your document is purely theoretical and provides only “basic technical information”, please indicate this section as “No technical data provided”.

## Section Title

### Sub title

### Sub title

### Sub title

## Section Title

### Sub title

### Sub title

### Sub title

## Section Title

### Sub title

### Sub title

### Sub title

# License

This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit https://creativecommons.org/licenses/by-sa/4.0/legalcode or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.