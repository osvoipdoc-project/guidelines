Open Source VoIP Documentation Project

# Abstract 

The purpose of this section will be to provide understanding of this project, 
the associated sub-projects and the various guidelines for contribution.

Since the inception of Open Source telephony, multiple projects had initiated.
Ranging from the historic GnuGK and Bayonne up to Asterisk and Freeswitch, 
the community contributed documentation for these was mostly located at 
http://www.voip-info.org. While the previously mentioned source still exists
and others, the information in these is less updated and under curated. This 
situation results in lack of proper guidance and propagation of best practice
techniques, in regards to Real Life Open Source VoIP implementations. 

Our objective is to provide an updated, curated and highly usable inventory of 
documents, detailing the various deployment scenarios and possibilities. We
believe that documentation should not include only configuration files or source
code, but actually include overviews and rationales for the implementation example
provided by the documentation. The documents provided are curated, thus, before
a document had been entered or accepted to the repository, it is examined by 
the relevant curator, to validate the information and its applicability to the 
project.

# Contribution

Contributions to the project should follow the example set forth in the 
CONTRIBUTION.md file. A contribution will be accepted to the project, as long as 
it will abide by the guidelines indicated in the above mentioned document. 

All contributions will be accepted under the same Creative Commons license of the 
project. We will not accept contributions that will require a different license.

We will accept contribution from commercial sources, as long as they are in direct
relation to an Open Source VoIP project. Thus, a contribution that is related to
a commercial product only will be rejected.

# Governance

The Open Source VoIP Documentation Project is a community effort and welcomes 
any able person who is willing to contribute. The project is divided into sub-projects
and sub-groups. Contributions should be performed to the "contributions" sub-project,
enabling the curators to examine the various contributions. Once a contribution 
had been fully accepted, it shall be moved to the primary project repositories.
Updates to the existing documentation shall be accepted via "pull-requests", as 
with any other GIT enabled project.

The objective is to keep the contribution process as simple as possible, while 
still allowing for proper curation to occur. 

# Core Team Members
  - Nir Simionovich
  - Lenz Emilitri
  - Sean C. McCord

# Contributing Members

# Curators
  - Nir Simionovich

# Content Marshals

# LICENSE
This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit https://creativecommons.org/licenses/by-sa/4.0/legalcode or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.